/**
 * Buffer
 * 
 * Autor: Diogo Reis Pavan
 * Ultima modificacao: 30/03/2018
 */
package exercicio4;

public interface Buffer {

    public void set (int value, String taskName, int ordem)throws InterruptedException;
    
    public int get ( String taskName, int ordem) throws InterruptedException;
    
    public boolean verificaOrdem(int ordem) throws InterruptedException;
}
