/**
 * Exemplo1: Programacao com threads
 * Autor: Diogo Reis Pavan
 * Ultima modificacao: 30/03/2018
 */
package exercicio4;

import java.util.Random;

/**
 *
 * @author Lucio
 */
public class PrintTasks implements Runnable {

    //Tempo aleatorio em que cada thread entra em sleep
    private final static Random generator = new Random();
    private final int sleepTime; //tempo de adormecimento aleatorio para a thread
    private final String taskName; //nome da tarefa    

    private int cor = 1;
    private int ordem;
    private final Buffer buffer;
    
    public PrintTasks(Buffer bufferCompartilhado , String name, int ordem){
        buffer = bufferCompartilhado;
        this.ordem = ordem;
        this.taskName = name;
        this.sleepTime = 2000; //milissegundos
    }
    
    public void run() {
        try {
            buffer.set(cor, taskName, ordem);
            while (true) {
                switch (cor) {
                    case 1:
                        //Tenho: verde                        
                        buffer.set(cor++, taskName, ordem);                       
                        break;
                    case 2:
                        buffer.set(1, taskName, ordem);
//                        Thread.yield();
                        break;
                        
                    default:
                        break;
                }
                //Estado de ESPERA SINCRONIZADA
                //Nesse ponto, a thread perde o processador, e permite que
                //outra thread execute
                cor = buffer.get(taskName, ordem);
                Thread.sleep(2000); //Deixa os carros passarem
                //Thread.sleep(2000);
                
            }//fim while

        } catch (Exception ex) {
            System.out.printf("%s %s\n", taskName, "terminou de maneira inesperada.");
        }
        
    }//fim run

}//fim classe
