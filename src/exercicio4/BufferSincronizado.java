/**
 * Exemplo de BufferSincronizado com métodos
 * wait(), notify() e notifyAll()
 *
 * Autor: Diogo Reis Pavan
 * Ultima modificacao: 01/04/2018
 */
package exercicio4;

import java.time.LocalDateTime;
import java.util.Random;

/**
 *
 * @author Lucio
 */
public class BufferSincronizado implements Buffer {

    //Tempo aleatorio em que cada thread entra em sleep
    private final static Random generator = new Random();

    private int buffer = -1; // compartilhado por todas as threads
    private boolean temValor = false; //indica se o buffer temValor
    private int ordemCor;

    //Insere o valor no buffer
    public synchronized void set(int valor, String taskName, int ordem) throws InterruptedException {

        if (this.verificaOrdem(ordem)) {
            if (!temValor) {
                buffer = valor; //insere um valor no buffer
                temValor = true;
                ordemCor = ordem;
                System.out.println(taskName + " - Verde - " + LocalDateTime.now());
            } else {
                System.out.println(taskName + " - Vermelho - " + LocalDateTime.now());
                Thread.sleep(generator.nextInt(300));
            }
        }
    }

    public synchronized int get(String taskName, int ordem) throws InterruptedException {

        if (this.verificaOrdem(ordem)) {
            if (temValor) {
                temValor = false;
                System.out.println(taskName + " - Vermelho - " + LocalDateTime.now());
            } else {
                System.out.println(taskName + " - Verde - " + LocalDateTime.now());
                Thread.sleep(generator.nextInt(300));
            }
        }

        return buffer;

    }//fim get

    @Override
    public synchronized boolean verificaOrdem(int ordem) throws InterruptedException {

        switch (ordemCor) {
            case 1:
                return ordem == 2 || ordemCor == ordem;
            case 2:
                return ordem == 3 || ordemCor == ordem;
            case 3:
                return ordem == 1 || ordemCor == ordem;
            default:
                return true;
        }

    } //fim verificaOrdem

}//fim classe
